package com.cse24gmail.jakir.bbmp.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.TextView;

import com.cse24gmail.jakir.bbmp.MusicController;
import com.cse24gmail.jakir.bbmp.NotificationMusic;
import com.cse24gmail.jakir.bbmp.R;
import com.cse24gmail.jakir.bbmp.TBMP;
import com.cse24gmail.jakir.bbmp.adapters.AdapterSong;

public class ActivityNowPlaying extends Activity implements
        MediaPlayerControl, OnItemClickListener {

    private ListView songListView;
    private boolean paused = false;
    private boolean playbackPaused = false;
    private MusicController musicController;
    private AdapterSong songAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_now_playing);

        songListView = (ListView) findViewById(R.id.activity_now_playing_song_list);
        TBMP.musicService.setList(TBMP.nowPlayingList);
        TBMP.musicService.setSong(0);

        songAdapter = new AdapterSong(this, TBMP.nowPlayingList);
        songListView.setAdapter(songAdapter);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            if (bundle.containsKey("song")) {
                int songToPlayIndex = bundle.getInt("song");
                TBMP.musicService.setSong(songToPlayIndex);
            }
            TBMP.musicService.playSong();
        }
        // Scroll the list view to the current song.
        songListView.setSelection(TBMP.musicService.currentSongPosition);

        songListView.setOnItemClickListener(this);
        setMusicController();

        if (playbackPaused) {
            setMusicController();
            playbackPaused = false;
        }

        ActivitySplashScreen.addNowPlayingItem(this);
        createActionBar();
    }
    private void createActionBar() {

        ActionBar actionBar = getActionBar();
        if (actionBar == null) {
            return;
        }
        actionBar.setHomeButtonEnabled(false);
        actionBar.setCustomView(R.layout.activity_now_playing_action_bar);

        TextView textTop = (TextView) actionBar.getCustomView().findViewById(R.id.action_bar_title);
        textTop.setText(getString(R.string.now_playing));

        TextView textBottom = (TextView) actionBar.getCustomView().findViewById(R.id.action_bar_subtitle);
        textBottom.setText("");
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
    }
    private MenuItem shuffleItem;
    private MenuItem repeatItem;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_menu_now_playing_action_bar, menu);

        shuffleItem = menu.findItem(R.id.action_bar_shuffle);
        repeatItem = menu.findItem(R.id.action_bar_repeat);

        refreshActionBarItems();
        refreshActionBarSubtitle();

        return super.onCreateOptionsMenu(menu);
    }
    private void refreshActionBarItems() {

        shuffleItem
                .setIcon((TBMP.musicService.isShuffle()) ? R.drawable.ic_menu_shuffle_on
                        : R.drawable.ic_menu_shuffle_off);

        repeatItem
                .setIcon((TBMP.musicService.isRepeat()) ? R.drawable.ic_menu_repeat_on
                        : R.drawable.ic_menu_repeat_off);
    }

    public void refreshActionBarSubtitle() {

        ActionBar actionBar = getActionBar();
        if (actionBar == null)
            return;

        if (TBMP.musicService.currentSong == null)
            return;

        TextView textBottom = (TextView) actionBar.getCustomView()
                .findViewById(R.id.action_bar_subtitle);
        textBottom.setText(TBMP.musicService.currentSong.getTitle());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_bar_shuffle:
                TBMP.musicService.toggleShuffle();
                refreshActionBarItems();
                return true;

            case R.id.action_bar_repeat:
                TBMP.musicService.toggleRepeat();
                refreshActionBarItems();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN)
            if (keyCode == KeyEvent.KEYCODE_MENU)
                musicController.show();

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onPause() {
        super.onPause();
        paused = true;
        playbackPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshActionBarSubtitle();
        if (paused) {
            setMusicController();
            paused = false;
        }

    }

    @Override
    protected void onStop() {
        musicController.hide();
        super.onStop();
    }

    private void setMusicController() {
        musicController = new MusicController(ActivityNowPlaying.this);
        musicController.setPrevNextListeners(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Calling method defined on ActivityNowPlaying
                playNext();
            }
        }, new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Calling method defined on ActivityNowPlaying
                playPrevious();
            }
        });

        // Binding to our media player
        musicController.setMediaPlayer(this);
        musicController
                .setAnchorView(findViewById(R.id.activity_now_playing_song_list));
        musicController.setEnabled(true);
    }

    @Override
    public void start() {
        TBMP.musicService.unpausePlayer();
    }
    @Override
    public void pause() {
        TBMP.musicService.pausePlayer();
    }

    @Override
    public int getDuration() {
        if (TBMP.musicService != null && TBMP.musicService.musicBound
                && TBMP.musicService.isPlaying())
            return TBMP.musicService.getDuration();
        else
            return 0;
    }

    @Override
    public int getCurrentPosition() {
        if (TBMP.musicService != null && TBMP.musicService.musicBound
                && TBMP.musicService.isPlaying())
            return TBMP.musicService.getPosition();
        else
            return 0;
    }

    @Override
    public void seekTo(int position) {
        TBMP.musicService.seekTo(position);
    }

    @Override
    public boolean isPlaying() {
        if (TBMP.musicService != null && TBMP.musicService.musicBound)
            return TBMP.musicService.isPlaying();

        return false;
    }

    @Override
    public int getBufferPercentage() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        // TODO Auto-generated method stub
        return 0;
    }

    public void playNext() {
        TBMP.musicService.next(true);
        TBMP.musicService.playSong();
        refreshActionBarSubtitle();

        if (playbackPaused) {
            setMusicController();
            playbackPaused = false;
        }
        musicController.show();
    }

    public void playPrevious() {
        TBMP.musicService.previous(true);
        TBMP.musicService.playSong();

        refreshActionBarSubtitle();

        if (playbackPaused) {
            setMusicController();
            playbackPaused = false;
        }
        musicController.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TBMP.musicService.setSong(position);
        songListView.setSelection(position);
        TBMP.musicService.playSong();
        refreshActionBarSubtitle();

        if (playbackPaused) {
            setMusicController();
            playbackPaused = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NotificationMusic.cancelAll(this);
        TBMP.stopMusicService(this);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alert = new AlertDialog.Builder(ActivityNowPlaying.this);
        alert.setMessage("Are you want to exit ?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,
                                int which) {
                finish();

            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                moveTaskToBack(false);
            }
        });
        alert.show();
    }
}

