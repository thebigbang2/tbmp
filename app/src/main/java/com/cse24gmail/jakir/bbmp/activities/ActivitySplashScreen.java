package com.cse24gmail.jakir.bbmp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ProgressBar;

import com.cse24gmail.jakir.bbmp.R;
import com.cse24gmail.jakir.bbmp.TBMP;

public class ActivitySplashScreen extends Activity{
    private ProgressBar progressBar;

	@Override
	protected void onCreate(Bundle seventhSonOfASeventhSon) {

		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		TBMP.settings.load(this);
		super.onCreate(seventhSonOfASeventhSon);
		setContentView(R.layout.activity_splash_screen);

        progressBar= (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setProgress(0);
        progressBar.setMax(30);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    while (progressBar.getProgress()<=progressBar.getMax()){
                        Thread.sleep(100);
                        if(progressBar.getProgress()==progressBar.getMax()){
                            TBMP.musicList = TBMP.songs.songs;
                            Intent gotoMainActivity=new Intent(ActivitySplashScreen.this,ActivityListSongs.class);
                            startActivity(gotoMainActivity);
                            finish();
                            break;
                        }
                        progressBar.incrementProgressBy(1);
                    }

                }catch (Exception e){

                }
            }
        }).start();


        TBMP.initialize(this);

		scanSongs(false);
	}

    void scanSongs(boolean forceScan) {

        if ((forceScan) || (! TBMP.songs.isInitialized())) {

            new ScanSongs().execute();
        }
    }
		@Override
	public void onBackPressed() {
        finish();
        TBMP.stopMusicService(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		TBMP.startMusicService(this);
	}

	class ScanSongs extends AsyncTask<String, Integer, String> {
		@Override
		protected String doInBackground(String... params) {

			try {
				// Will scan all songs on the device
				TBMP.songs.scanSongs(ActivitySplashScreen.this, "external");
				return ActivitySplashScreen.this.getString(R.string.menu_main_scanning_ok);
			}
			catch (Exception e) {
				Log.e("Couldn't execute background task", e.toString());
				e.printStackTrace();
				return ActivitySplashScreen.this.getString(R.string.menu_main_scanning_not_ok);
			}
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

		}
	}

	public static void addNowPlayingItem(Context c) {

		if (TBMP.mainMenuHasNowPlayingItem)
			return;
		TBMP.mainMenuHasNowPlayingItem = true;
	}
}
