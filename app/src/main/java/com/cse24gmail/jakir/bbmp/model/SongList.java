package com.cse24gmail.jakir.bbmp.model;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class SongList {
    public ArrayList<Song> songs = new ArrayList<Song>();
    private HashMap<String, String> genreIdToGenreNameMap;
    private HashMap<String, String> songIdToGenreIdMap;
    private boolean scannedSongs;
    private boolean scanningSongs;

    public boolean isInitialized() {
        return scannedSongs;
    }

    public void scanSongs(Context c, String fromWhere) {
        if (fromWhere == "both")
            throw new RuntimeException("Can't scan from both locations - not implemented");

        if (scanningSongs)
            return;
        scanningSongs = true;

        Uri musicUri = ((fromWhere == "internal") ?
                MediaStore.Audio.Media.INTERNAL_CONTENT_URI :
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        Uri genreUri = ((fromWhere == "internal") ?
                MediaStore.Audio.Genres.INTERNAL_CONTENT_URI :
                MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI);

        // Gives us access to query for files on the system.
        ContentResolver resolver = c.getContentResolver();

        Cursor cursor;

        String GENRE_ID = MediaStore.Audio.Genres._ID;
        String GENRE_NAME = MediaStore.Audio.Genres.NAME;
        String SONG_ID = MediaStore.Audio.Media._ID;
        String SONG_TITLE = MediaStore.Audio.Media.TITLE;
        String SONG_ARTIST = MediaStore.Audio.Media.ARTIST;
        String SONG_ALBUM = MediaStore.Audio.Media.ALBUM;
        String SONG_YEAR = MediaStore.Audio.Media.YEAR;
        String SONG_TRACK_NO = MediaStore.Audio.Media.TRACK;
        String SONG_FILEPATH = MediaStore.Audio.Media.DATA;
        String SONG_DURATION = MediaStore.Audio.Media.DURATION;

        genreIdToGenreNameMap = new HashMap<String, String>();
        String[] genreColumns = {
                GENRE_ID,
                GENRE_NAME
        };

        cursor = resolver.query(genreUri, genreColumns, null, null, null);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
            genreIdToGenreNameMap.put(cursor.getString(0), cursor.getString(1));

        cursor.close();
        // Map from Songs IDs to Genre IDs
        songIdToGenreIdMap = new HashMap<String, String>();
        // UPDATE URI HERE
        if (fromWhere == "both")
            throw new RuntimeException("Can't scan from both locations - not implemented");
        // For each genre, we'll query the databases to get
        // all songs's IDs that have it as a genre.
        for (String genreID : genreIdToGenreNameMap.keySet()) {

            Uri uri = MediaStore.Audio.Genres.Members.getContentUri(fromWhere,
                    Long.parseLong(genreID));

            cursor = resolver.query(uri, new String[]{SONG_ID}, null, null, null);

            // Iterating through the results, populating the map
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

                long currentSongID = cursor.getLong(cursor.getColumnIndex(SONG_ID));

                songIdToGenreIdMap.put(Long.toString(currentSongID), genreID);
            }
            cursor.close();
        }

        String[] columns = {
                SONG_ID,
                SONG_TITLE,
                SONG_ARTIST,
                SONG_ALBUM,
                SONG_YEAR,
                SONG_TRACK_NO,
                SONG_FILEPATH,
                SONG_DURATION
        };

        final String musicsOnly = MediaStore.Audio.Media.IS_MUSIC + "=1";
        // Actually querying the system
        cursor = resolver.query(musicUri, columns, musicsOnly, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                // Creating a song from the values on the row
                Song song = new Song(cursor.getInt(cursor.getColumnIndex(SONG_ID)),
                        cursor.getString(cursor.getColumnIndex(SONG_FILEPATH)));

                song.setTitle(cursor.getString(cursor.getColumnIndex(SONG_TITLE)));
                song.setArtist(cursor.getString(cursor.getColumnIndex(SONG_ARTIST)));
                song.setAlbum(cursor.getString(cursor.getColumnIndex(SONG_ALBUM)));

                song.setTrackNumber(cursor.getInt(cursor.getColumnIndex(SONG_TRACK_NO)));
                song.setDuration(cursor.getInt(cursor.getColumnIndex(SONG_DURATION)));

                songs.add(song);
            }
            while (cursor.moveToNext());
        } else {
            // What do I do if I can't find any songs?
        }
        cursor.close();

        // based on the song title.
        Collections.sort(songs, new Comparator<Song>() {
            public int compare(Song a, Song b) {
                return a.getTitle().compareTo(b.getTitle());
            }
        });

        scannedSongs = true;
        scanningSongs = false;
    }
    public Song getSongById(long id) {

        Song currentSong = null;

        for (Song song : songs)
            if (song.getId() == id) {
                currentSong = song;
                break;
            }

        return currentSong;
    }
}
