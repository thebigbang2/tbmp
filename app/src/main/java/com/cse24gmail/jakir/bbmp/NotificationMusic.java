package com.cse24gmail.jakir.bbmp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.cse24gmail.jakir.bbmp.activities.ActivityNowPlaying;
import com.cse24gmail.jakir.bbmp.model.Song;

public class NotificationMusic{
	Context context = null;
	Service service = null;
	Notification.Builder notificationBuilder = null;
    protected int NOTIFICATION_ID=1;
	RemoteViews notificationView = null;
	NotificationManager notificationManager = null;
	public void notifySong(Context context, Service service, Song song) {

		if (this.context == null)
			this.context = context;
		if (this.service == null)
			this.service = service;
		Intent notifyIntent = new Intent(context, ActivityNowPlaying.class);
		notifyIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent pendingIntent = PendingIntent.getActivity
				(context,
				 0,
				 notifyIntent,
				 PendingIntent.FLAG_UPDATE_CURRENT);
		notificationView = new RemoteViews(TBMP.packageName, R.layout.notification);
		notificationView.setImageViewResource(R.id.notification_button_play, R.drawable.pause);
		notificationView.setImageViewResource(R.id.notification_button_skip, R.drawable.skip);
		notificationView.setTextViewText(R.id.notification_text_title, song.getTitle());
		notificationView.setTextViewText(R.id.notification_text_artist, song.getArtist());

		Intent buttonPlayIntent = new Intent(context, NotificationPlayButtonHandler.class);
		buttonPlayIntent.putExtra("action", "togglePause");

		PendingIntent buttonPlayPendingIntent = PendingIntent.getBroadcast(context, 0, buttonPlayIntent, 0);
		notificationView.setOnClickPendingIntent(R.id.notification_button_play, buttonPlayPendingIntent);

		Intent buttonSkipIntent = new Intent(context, NotificationSkipButtonHandler.class);
		buttonSkipIntent.putExtra("action", "skip");

		PendingIntent buttonSkipPendingIntent = PendingIntent.getBroadcast(context, 0, buttonSkipIntent, 0);
		notificationView.setOnClickPendingIntent(R.id.notification_button_skip, buttonSkipPendingIntent);

		notificationBuilder = new Notification.Builder(context);

		notificationBuilder.setContentIntent(pendingIntent)
		                   .setSmallIcon(R.drawable.ic_launcher)
		                   .setTicker("TBMP: Playing '" + song.getTitle() + "' from '" + song.getArtist() + "'")
		                   .setOngoing(true)
		                   .setContentTitle(song.getTitle())
		                   .setContentText(song.getArtist())
		                   .setContent(notificationView);

		Notification notification = notificationBuilder.build();

		notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

		service.startForeground(NOTIFICATION_ID, notification);
	}

	public static class NotificationPlayButtonHandler extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			TBMP.musicService.togglePlayback();
		}
	}

	public static class NotificationSkipButtonHandler extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			TBMP.musicService.next(true);
			TBMP.musicService.playSong();
		}
	}

	public void notifyPaused(boolean isPaused) {
		if ((notificationView == null) || (notificationBuilder == null))
			return;

		int iconID = ((isPaused)?
	                  R.drawable.play :
	                  R.drawable.pause);

		notificationView.setImageViewResource(R.id.notification_button_play, iconID);

		notificationBuilder.setContent(notificationView);
		service.startForeground(NOTIFICATION_ID, notificationBuilder.build());
	}

	public void cancel() {
		service.stopForeground(true);

		notificationManager.cancel(NOTIFICATION_ID);
	}
	public static void cancelAll(Context c) {
		NotificationManager manager = (NotificationManager)c.getSystemService(Context.NOTIFICATION_SERVICE);
		manager.cancelAll();
	}
}
