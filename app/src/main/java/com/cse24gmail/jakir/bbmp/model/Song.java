package com.cse24gmail.jakir.bbmp.model;

public class Song {

	private long id;
	private String filePath;

	public Song(long id, String filePath) {
		this.id        = id;
		this.filePath  = filePath;
	}

	public long getId() {
		return id;
	}

	// optional metadata

	private String title       = "";
	private String artist      = "";
	private String album       = "";
	private int    track_no    = -1;
	private long   duration_ms = -1;


	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getAlbum() {
		return album;
	}
	public void setAlbum(String album) {
		this.album = album;
	}

	public void setTrackNumber(int track_no) {
		this.track_no = track_no;
	}

	public void setDuration(long duration_ms) {
		this.duration_ms = duration_ms;
	}
	public long getDuration() {
		return duration_ms;
	}

    public long getDurationSeconds() {
		return getDuration() / 1000;
	}
}
