package com.cse24gmail.jakir.bbmp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.IBinder;

import com.cse24gmail.jakir.bbmp.model.Song;
import com.cse24gmail.jakir.bbmp.model.SongList;
import com.cse24gmail.jakir.bbmp.services.ServicePlayMusic;
import com.cse24gmail.jakir.bbmp.services.ServicePlayMusic.MusicBinder;

import java.util.ArrayList;

public class TBMP {
	public static SongList songs = new SongList();
	public static Settings settings = new Settings();
	public static ServicePlayMusic musicService = null;
	public static ArrayList<Song> musicList = null;
	public static ArrayList<Song> nowPlayingList = null;
	public static boolean mainMenuHasNowPlayingItem = false;
	// GENERAL PROGRAM INFO
	public static String applicationName = "TBM Player";
	public static String packageName = "<unknown>";
	public static String versionName = "<unknown>";
	public static int    versionCode = -1;
	public static long   firstInstalledTime = -1;
	public static long   lastUpdatedTime    = -1;

	public static void initialize(Context c) {

		TBMP.packageName = c.getPackageName();

		try {
			// Retrieving several information
			PackageInfo info = c.getPackageManager().getPackageInfo(TBMP.packageName, 0);

			TBMP.versionName        = info.versionName;
			TBMP.versionCode        = info.versionCode;
			TBMP.firstInstalledTime = info.firstInstallTime;
			TBMP.lastUpdatedTime    = info.lastUpdateTime;

		} catch (PackageManager.NameNotFoundException e) {

		}
	}
	public static ServiceConnection musicConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			MusicBinder binder = (MusicBinder)service;

			// Here's where we finally create the MusicService
			musicService = binder.getService();
			musicService.setList(TBMP.songs.songs);
			musicService.musicBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			musicService.musicBound = false;
		}
	};

	private static Intent musicServiceIntent = null;

	public static void startMusicService(Context c) {

		if (musicServiceIntent != null)
			return;

		if (TBMP.musicService != null)
			return;

		musicServiceIntent = new Intent(c, ServicePlayMusic.class);
		c.bindService(musicServiceIntent, musicConnection, Context.BIND_AUTO_CREATE);
		c.startService(musicServiceIntent);
	}

	public static void stopMusicService(Context c) {

		if (musicServiceIntent == null)
			return;

		c.stopService(musicServiceIntent);
		musicServiceIntent = null;

		TBMP.musicService = null;
	}

}
