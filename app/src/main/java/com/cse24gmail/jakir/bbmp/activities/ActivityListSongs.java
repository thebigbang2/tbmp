package com.cse24gmail.jakir.bbmp.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.cse24gmail.jakir.bbmp.R;
import com.cse24gmail.jakir.bbmp.TBMP;
import com.cse24gmail.jakir.bbmp.adapters.AdapterSong;

public class ActivityListSongs extends Activity
        implements OnItemClickListener {

    private ListView songListView;

    @Override
    protected void onCreate(Bundle popcorn) {
        super.onCreate(popcorn);
        setContentView(R.layout.activity_list_songs);

        songListView = (ListView) findViewById(R.id.activity_list_songs_list);
        songListView.setOnItemClickListener(this);

        // If we got an extra with a title, we'll apply it
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null)
            this.setTitle((String) bundle.get("title"));

        // Connects the song list to an adapter
        // (thing that creates several Layouts from the song list)
        if ((TBMP.musicList != null) && (!TBMP.musicList.isEmpty())) {
            AdapterSong songAdapter = new AdapterSong(this, TBMP.musicList);
            songListView.setAdapter(songAdapter);
        }

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        // We'll play the current song list
        TBMP.nowPlayingList = TBMP.musicList;

        Intent intent = new Intent(this, ActivityNowPlaying.class);
        intent.putExtra("song", position);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
