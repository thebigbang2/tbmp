package com.cse24gmail.jakir.bbmp.adapters;

import java.util.ArrayList;

import com.cse24gmail.jakir.bbmp.R;
import com.cse24gmail.jakir.bbmp.model.Song;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AdapterSong extends BaseAdapter {

    private ArrayList<Song> songs;
    private LayoutInflater songInflater;

    public AdapterSong(Context c, ArrayList<Song> theSongs) {
        songs = theSongs;
        songInflater = LayoutInflater.from(c);
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int position) {
        return songs.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Will map from a Song to a Song layout
        LinearLayout songLayout = (LinearLayout) songInflater.inflate(R.layout.menu_item_song, parent, false);

        TextView titleView = (TextView) songLayout.findViewById(R.id.menu_item_song_title);
        TextView albumView = (TextView) songLayout.findViewById(R.id.menu_item_song_album);

        Song currentSong = songs.get(position);

        String title = currentSong.getTitle();
        if (title.isEmpty())
            titleView.setText("<unknown>");
        else
            titleView.setText(currentSong.getTitle());

        String album = currentSong.getAlbum();
        if (album.isEmpty())
            albumView.setText("<unknown>");
        else
            albumView.setText(currentSong.getAlbum());

        songLayout.setTag(position);
        return songLayout;
    }
}
