package com.cse24gmail.jakir.bbmp;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Settings {

    private SharedPreferences preferences = null;

    public void load(Context c) {
        preferences = PreferenceManager.getDefaultSharedPreferences(c);
    }

    public boolean get(String key, boolean defaultValue) {
        if (preferences == null)
            return defaultValue;

        return preferences.getBoolean(key, defaultValue);
    }

    public String get(String key, String defaultValue) {
        if (preferences == null)
            return defaultValue;

        return preferences.getString(key, defaultValue);
    }
}
