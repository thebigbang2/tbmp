package com.cse24gmail.jakir.bbmp.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.cse24gmail.jakir.bbmp.NotificationMusic;
import com.cse24gmail.jakir.bbmp.TBMP;
import com.cse24gmail.jakir.bbmp.model.Song;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class ServicePlayMusic extends Service
        implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener{

    public static final String BROADCAST_ACTION = "action";
    public static final String BROADCAST_EXTRA_STATE = "state";
    public static final String BROADCAST_EXTRA_SONG_ID = "songId";
    public static final String BROADCAST_EXTRA_PLAYING = "playing";
    public static final String BROADCAST_EXTRA_PAUSED = "paused";
    public static final String BROADCAST_EXTRA_UNPAUSED = "unpaused";
    public static final String BROADCAST_EXTRA_COMPLETED = "completed";
    public static final String BROADCAST_EXTRA_SKIP_NEXT = "skip_next";
    public static final String BROADCAST_EXTRA_SKIP_PREVIOUS = "skip_previous";

    private MediaPlayer player;
    private ArrayList<Song> songs;
    public int currentSongPosition;
    public Song currentSong = null;
    private boolean shuffleMode = false;
    private Random randomNumberGenerator;
    private boolean repeatMode = false;
    private NotificationMusic notification = null;
    final static String TAG = "MusicService";

    public static final String BROADCAST_ORDER = "com.thebigbang.MUSIC_SERVICE";
    public static final String BROADCAST_EXTRA_GET_ORDER = "com.thebigbang.musicplayer.MUSIC_SERVICE";

    public static final String BROADCAST_ORDER_PLAY = "com.thebigbang.musicplayer.action.PLAY";
    public static final String BROADCAST_ORDER_PAUSE = "com.thebigbang.musicplayer.action.PAUSE";
    public static final String BROADCAST_ORDER_TOGGLE_PLAYBACK = "com.thebigbang.action.musicplayer.PLAYBACK";
    public static final String BROADCAST_ORDER_SKIP = "com.thebigbang.musicplayer.action.SKIP";
    public static final String BROADCAST_ORDER_REWIND = "com.thebigbang.musicplayer.action.REWIND";

    enum ServiceState {
        Stopped,
        Preparing,
        Playing,
        Paused
    }

    ServiceState serviceState = ServiceState.Preparing;

    public void onCreate() {
        super.onCreate();
        currentSongPosition = 0;
        randomNumberGenerator = new Random();
        initMusicPlayer();
        LocalBroadcastManager
                .getInstance(getApplicationContext())
                .registerReceiver(localBroadcastReceiver, new IntentFilter(ServicePlayMusic.BROADCAST_ORDER));
    }

    public void initMusicPlayer() {
        if (player == null)
            player = new MediaPlayer();

        // even when the device is sleeping.
        player.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);

        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        player.setOnPreparedListener(this); // player initialized
        player.setOnCompletionListener(this); // song completed
        player.setOnErrorListener(this);
    }

    public void stopMusicPlayer() {
        if (player == null)
            return;
        player.stop();
        player.release();
        player = null;
    }

    public void setList(ArrayList<Song> theSongs) {
        songs = theSongs;
    }

    BroadcastReceiver localBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String order = intent.getStringExtra(ServicePlayMusic.BROADCAST_EXTRA_GET_ORDER);
            if (order == null)
                return;

            if (order.equals(ServicePlayMusic.BROADCAST_ORDER_PAUSE)) {
                pausePlayer();
            } else if (order.equals(ServicePlayMusic.BROADCAST_ORDER_PLAY)) {
                unpausePlayer();
            } else if (order.equals(ServicePlayMusic.BROADCAST_ORDER_TOGGLE_PLAYBACK)) {
                togglePlayback();
            } else if (order.equals(ServicePlayMusic.BROADCAST_ORDER_SKIP)) {
                next(true);
                playSong();
            } else if (order.equals(ServicePlayMusic.BROADCAST_ORDER_REWIND)) {
                previous(true);
                playSong();
            }
        }
    };

    @Override
    public void onPrepared(MediaPlayer mp) {
        serviceState = ServiceState.Playing;
        // Start playback
        player.start();
        notifyCurrentSong();
    }

    public void setSong(int songIndex) {
        if (songIndex < 0 || songIndex >= songs.size())
            currentSongPosition = 0;
        else
            currentSongPosition = songIndex;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        // Keep this state!
        serviceState = ServiceState.Playing;
        broadcastState(ServicePlayMusic.BROADCAST_EXTRA_COMPLETED);
        // Repeating current song if desired
        if (repeatMode) {
            playSong();
            return;
        }
        next(false);
        if (currentSongPosition == 0) {
            if (TBMP.settings.get("repeat_list", false))
                playSong();
            else
                destroySelf();
            return;
        }
        playSong();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mp.reset();
        return false;
    }

    @Override
    public void onDestroy() {
        cancelNotification();
        currentSong = null;

        stopMusicPlayer();
        super.onDestroy();
    }

    private void destroySelf() {
        stopSelf();
        currentSong = null;
    }

    public void previous(boolean userSkippedSong) {
        if (serviceState != ServiceState.Paused && serviceState != ServiceState.Playing)
            return;

        if (userSkippedSong)
            broadcastState(ServicePlayMusic.BROADCAST_EXTRA_SKIP_PREVIOUS);
        currentSongPosition--;
        if (currentSongPosition < 0)
            currentSongPosition = songs.size() - 1;
    }

    public void next(boolean userSkippedSong) {
        if (serviceState != ServiceState.Paused && serviceState != ServiceState.Playing)
            return;

        if (userSkippedSong)
            broadcastState(ServicePlayMusic.BROADCAST_EXTRA_SKIP_NEXT);
        if (shuffleMode) {
            int newSongPosition = currentSongPosition;
            while (newSongPosition == currentSongPosition)
                newSongPosition = randomNumberGenerator.nextInt(songs.size());
            currentSongPosition = newSongPosition;
            return;
        }
        currentSongPosition++;
        if (currentSongPosition >= songs.size())
            currentSongPosition = 0;
    }

    public int getPosition() {
        return player.getCurrentPosition();
    }

    public int getDuration() {
        return player.getDuration();
    }

    public boolean isPlaying() {
        boolean returnValue = false;
        try {
            returnValue = player.isPlaying();
        } catch (IllegalStateException e) {
            player.reset();
            player.prepareAsync();
        }
        return returnValue;
    }

    public void playSong() {
        player.reset();
        Song songToPlay = songs.get(currentSongPosition);
        currentSong = songToPlay;
        // Append the external URI with our songs'
        Uri songToPlayURI = ContentUris.withAppendedId
                (android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        songToPlay.getId());

        try {
            player.setDataSource(getApplicationContext(), songToPlayURI);
        } catch (IOException io) {
            Log.e(TAG, "IOException: couldn't change the song", io);
            destroySelf();
        } catch (Exception e) {
            Log.e(TAG, "Error when changing the song", e);
            destroySelf();
        }
        player.prepareAsync();
        serviceState = ServiceState.Preparing;
        broadcastState(ServicePlayMusic.BROADCAST_EXTRA_PLAYING);

    }

    public void pausePlayer() {
        if (serviceState != ServiceState.Paused && serviceState != ServiceState.Playing)
            return;
        player.pause();
        serviceState = ServiceState.Paused;
        notification.notifyPaused(true);
        broadcastState(ServicePlayMusic.BROADCAST_EXTRA_PAUSED);
    }

    public void unpausePlayer() {
        if (serviceState != ServiceState.Paused && serviceState != ServiceState.Playing)
            return;
        player.start();
        serviceState = ServiceState.Playing;
        notification.notifyPaused(false);
        broadcastState(ServicePlayMusic.BROADCAST_EXTRA_UNPAUSED);
    }

    public void togglePlayback() {
        if (serviceState == ServiceState.Paused)
            unpausePlayer();
        else
            pausePlayer();
    }

    public void seekTo(int position) {
        player.seekTo(position);
    }

    public void toggleShuffle() {
        shuffleMode = !shuffleMode;
    }

    public boolean isShuffle() {
        return shuffleMode;
    }
    public void toggleRepeat() {
        repeatMode = !repeatMode;
    }
    public boolean isRepeat() {
        return repeatMode;
    }
    public boolean musicBound = false;

    public class MusicBinder extends Binder {
        public ServicePlayMusic getService() {
            return ServicePlayMusic.this;
        }
    }
    private final IBinder musicBind = new MusicBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }
    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }
    public void notifyCurrentSong() {
        if (!TBMP.settings.get("show_notification", true))
            return;
        if (currentSong == null)
            return;
        if (notification == null)
            notification = new NotificationMusic();
        notification.notifySong(this, this, currentSong);
    }
    public void cancelNotification() {
        if (notification == null)
            return;
        notification.cancel();
        notification = null;
    }
    private void broadcastState(String state) {
        if (currentSong == null)
            return;
        Intent broadcastIntent = new Intent(ServicePlayMusic.BROADCAST_ACTION);
        broadcastIntent.putExtra(ServicePlayMusic.BROADCAST_EXTRA_STATE, state);
        broadcastIntent.putExtra(ServicePlayMusic.BROADCAST_EXTRA_SONG_ID, currentSong.getId());
        LocalBroadcastManager
                .getInstance(getApplicationContext())
                .sendBroadcast(broadcastIntent);
    }
}
